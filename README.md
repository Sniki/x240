# This is a quick installation guide of Arch Linux for Lenovo ThinkPad X240.

## Packages

### Xorg Packages
xorg xorg-server xorg-fonts-misc xorg-xbacklight

### Drivers

#### Intel Graphics HD4400
mesa libva-intel-driver vulkan-intel vulkan-icd-loader brightnessctl

#### Audio
pipewire pipewire-pulse pipewire-alsa pipewire-jack alsa-utils

#### Network
networkmanager modemmanager network-manager-applet 

#### Bluetooth
bluez bluez-utils blueman

#### File Systems
btrfs-progs xfsprogs exfatprogs dosfstools udftools fuseiso gvfs udisks2 udiskie acpi acpid

#### Printers
hplip cups

### Utilities

#### Shells & Prompts
bash-completion fish starship

#### Tools
rofi sddm xdg-user-dirs picom lxappearance dunst lxsession dialog nitrogen neofetch pacman-contrib alacritty htop

#### Archiving & Compression
p7zip zip unzip unrar

#### Developer Tools
base-devel wget git curl vim emacs geany

#### Themes
arc-gtk-theme

#### Icon Packs
arc-icon-theme elementary-icon-theme papirus-icon-theme

### Window Managers

#### Qtile
qtile python-psutil python-iwlib python-dbus-next pywlroots

### Fonts
ttf-ubuntu-font-family ttf-bitstream-vera ttf-dejavu ttf-font-awesome ttf-roboto ttf-crosscore ttf-joypixels ttf-hack noto-fonts ttf-caladea ttf-droid ttf-cascadia-code ttf-cormorant ttf-inconsolata

### Enable Services
NetworkManager ModemManager bluetooth acpid cups sddm 
