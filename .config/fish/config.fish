#  ____        _ _    _
# / ___| _ __ (_) | _(_)    Besnik Rrustemi (Sniki)
# \___ \| '_ \| | |/ / |    https://gitlab.com/sniki/
#  ___) | | | | |   <| |
# |____/|_| |_|_|_|\_\_|
#
# This is my Fish Shell config file with the stuff that i use.

# aliases #

# changing ls to exa #
alias ls='exa -al --color=always --group-directories-first'
alias l.='exa -a | grep -E "^\."'

# Starship Promt #
starship init fish | source

# Shell Color Scripts #
colorscript random
