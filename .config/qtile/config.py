#  ____        _ _    _
# / ___| _ __ (_) | _(_)    Besnik Rrustemi (Sniki)
# \___ \| '_ \| | |/ / |    https://gitlab.com/sniki/
#  ___) | | | | |   <| |
# |____/|_| |_|_|_|\_\_|
#
# This is my Qtile Window Manager config file with the stuff that i use.

# Imports
import os, subprocess
from libqtile import bar, extension, hook, layout, qtile, widget
from libqtile.config import Click, Drag, DropDown, Group, Key, KeyChord, Match, ScratchPad, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from qtile_extras import widget
from qtile_extras.widget.decorations import BorderDecoration, PowerLineDecoration, RectDecoration

# Defaults
mod = "mod4"
terminal = guess_terminal()

# Color Schemes
color_scheme = {
"doom-one":        [["#282c34"],  #  colors[0] - Black (Background)
                    ["#dfdfdf"],  #  colors[1] - White (Foreground)
                    ["#46d9ff"],  #  colors[2] - Cyan
                    ["#51afef"],  #  colors[3] - Blue
                    ["#a9a1e1"],  #  colors[4] - Purple
                    ["#c678dd"],  #  colors[5] - Pink
                    ["#ff6c6b"],  #  colors[6] - Red
                    ["#da8548"],  #  colors[7] - Orange
                    ["#ecbe7b"],  #  colors[8] - Yellow
                    ["#98be65"],  #  colors[9] - Green
                    ["#3e4556"],  # colors[10] - Separator / Border Normal
                    ["#46d9ff"]], # colors[11] - Border Focus

"dracula":         [["#282a36"],  #  colors[0] - Black (Background)
                    ["#f8f8f2"],  #  colors[1] - White (Foreground)
                    ["#8be9fd"],  #  colors[2] - Cyan
                    ["#6272a4"],  #  colors[3] - Blue
                    ["#bd93f9"],  #  colors[4] - Purple
                    ["#ff79c6"],  #  colors[5] - Pink
                    ["#ff5555"],  #  colors[6] - Red
                    ["#ffb86c"],  #  colors[7] - Orange
                    ["#f1fa8c"],  #  colors[8] - Yellow
                    ["#50fa7b"],  #  colors[9] - Green
                    ["#44475a"],  # colors[10] - Separator / Border Normal
                    ["#bd93f9"]], # colors[11] - Border Focus

"nord":            [["#2e3440"],  #  colors[0] - Black (Background)
                    ["#e5e9f0"],  #  colors[1] - White (Foreground)
                    ["#88c0d0"],  #  colors[2] - Cyan
                    ["#81a1c1"],  #  colors[3] - Blue
                    ["#b48ead"],  #  colors[4] - Purple
                    ["#8fbcbb"],  #  colors[5] - Teal
                    ["#bf616a"],  #  colors[6] - Red
                    ["#d08770"],  #  colors[7] - Orange
                    ["#ebcb8b"],  #  colors[8] - Yellow
                    ["#a3be8c"],  #  colors[9] - Green
                    ["#434c5e"],  # colors[10] - Separator/ Border Normal
                    ["#88c0d0"]], # colors[11] - Border Focus

"gruvbox-dark":    [["#282828"],  #  colors[0] - Black (Background)
                    ["#ebdbb2"],  #  colors[1] - White (Foreground)
                    ["#689d6a"],  #  colors[2] - Cyan
                    ["#458588"],  #  colors[3] - Blue
                    ["#b16286"],  #  colors[4] - Purple
                    ["#a89984"],  #  colors[5] - Gray
                    ["#cc241d"],  #  colors[6] - Red
                    ["#d65d0e"],  #  colors[7] - Orange
                    ["#d79921"],  #  colors[8] - Yellow
                    ["#98971a"],  #  colors[9] - Green
                    ["#504945"],  # colors[10] - Separator / Border Normal
                    ["#a89984"]], # colors[11] - Border Focus

"gruvbox-light":   [["#ebdbb2"],  #  colors[0] - Black (Background)
                    ["#3c3836"],  #  colors[1] - White (Foreground)
                    ["#427b58"],  #  colors[2] - Cyan
                    ["#076678"],  #  colors[3] - Blue
                    ["#8f3f71"],  #  colors[4] - Purple
                    ["#928374"],  #  colors[5] - Gray
                    ["#9d0006"],  #  colors[6] - Red
                    ["#d65d0e"],  #  colors[7] - Orange
                    ["#b57614"],  #  colors[8] - Yellow
                    ["#79740e"],  #  colors[9] - Green
                    ["#928374"],  # colors[10] - Separator / Border Normal
                    ["#a89984"]], # colors[11] - Border Focus

"solarized-dark":  [["#002b36"],  #  colors[0] - Black (Background)
                    ["#fdf6e3"],  #  colors[1] - White (Foreground)
                    ["#2aa198"],  #  colors[2] - Cyan
                    ["#268bd2"],  #  colors[3] - Blue
                    ["#6c71c4"],  #  colors[4] - Purple
                    ["#d33682"],  #  colors[5] - Pink
                    ["#dc322f"],  #  colors[6] - Red
                    ["#cb4b16"],  #  colors[7] - Orange
                    ["#b58900"],  #  colors[8] - Yellow
                    ["#859900"],  #  colors[9] - Green
                    ["#586e75"],  # colors[10] - Separator / Border Normal
                    ["#2aa198"]], # colors[11] - Border Focus

"solarized-light": [["#eee8d5"],  #  colors[0] - Black (Background)
                    ["#002b36"],  #  colors[1] - White (Foreground)
                    ["#2aa198"],  #  colors[2] - Cyan
                    ["#268bd2"],  #  colors[3] - Blue
                    ["#6c71c4"],  #  colors[4] - Purple
                    ["#d33682"],  #  colors[5] - Pink
                    ["#dc322f"],  #  colors[6] - Red
                    ["#cb4b16"],  #  colors[7] - Orange
                    ["#b58900"],  #  colors[8] - Yellow
                    ["#859900"],  #  colors[9] - Green
                    ["#fdf6e3"],  # colors[10] - Separator / Border Normal
                    ["#2aa198"]], # colors[11] - Border Focus
}

# Choose the colorscheme here
colors = color_scheme["gruvbox-dark"]

# Keybindings
keys = [
	# Lenovo ThinkPad Function Keys
	Key([], "XF86AudioMute", 
	    lazy.spawn("amixer sset Master toggle"), 
	    desc="Mute/UnMute Volume"
	),
	Key([], "XF86AudioLowerVolume", 
	    lazy.spawn("amixer sset Master 5%-"), 
	    desc="Lower Audio Volume"
	),
	Key([], "XF86AudioRaiseVolume", 
	    lazy.spawn("amixer sset Master 5%+"), 
	    desc="Increase Audio Volume"
	),
	Key([], "XF86AudioMicMute", 
	    lazy.spawn("amixer sset Capture toggle"), 
	    desc="Mute/UnMute Microphone"
	),
	Key([], "XF86MonBrightnessDown", 
	    lazy.spawn("brightnessctl set 5%-"), 
	    desc="Lower Screen Brightness"
	),
    Key([], "XF86MonBrightnessUp", 
        lazy.spawn("brightnessctl set +5%"), 
        desc="Increase Screen Brightness"
    ),
    Key([], "XF86Display", 
        lazy.spawn(""), 
        desc="Video Mirror"
    ),
    Key([], "XF86WLAN", 
        lazy.spawn(""), 
        desc="Wireless/Bluetooth Radio ON/OFF"
    ),
    Key([], "XF86Tools", 
        lazy.spawn("alacritty"), 
        desc="Settings"
    ),
    Key([], "XF86Search", 
        lazy.spawn("rofi -show run"), 
        desc="Search"
    ),
    Key([], "XF86LaunchA", 
        lazy.spawn("firefox"), 
        desc="Launcher"
    ),
    Key([], "XF86Explorer", 
        lazy.spawn("pcmanfm"), 
        desc="Explorer"
    ),
    Key([], "Print", 
        lazy.spawn("scrot '%Y-%m-%d_$wx$h.png' -e 'mv $f ~/Pictures/Screenshots/'"), 
        desc="Screenshot"
    ),
    Key([mod], "Print", 
        lazy.spawn("scrot -s '%Y-%m-%d_$wx$h.png' -e 'mv $f ~/Pictures/Screenshots/'"), 
        desc="Screenshot Selection"
    ),
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key([mod], "h", 
        lazy.layout.left(), 
        desc="Move focus to left"
    ),
    Key([mod], "l", 
        lazy.layout.right(), 
        desc="Move focus to right"
    ),
    Key([mod], "j", 
        lazy.layout.down(), 
        desc="Move focus down"
    ),
    Key([mod], "k", 
        lazy.layout.up(), 
        desc="Move focus up"
    ),
    Key([mod], "space", 
        lazy.layout.next(), 
        desc="Move window focus to other window"
    ),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", 
        lazy.layout.shuffle_left(), 
        desc="Move window to the left"
    ),
    Key([mod, "shift"], "l", 
        lazy.layout.shuffle_right(), 
        desc="Move window to the right"
    ),
    Key([mod, "shift"], "j", 
        lazy.layout.shuffle_down(), 
        desc="Move window down"
    ),
    Key([mod, "shift"], "k", 
        lazy.layout.shuffle_up(), 
        desc="Move window up"
    ),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", 
        lazy.layout.grow_left(), 
        desc="Grow window to the left"
    ),
    Key([mod, "control"], "l", 
        lazy.layout.grow_right(), 
        desc="Grow window to the right"
    ),
    Key([mod, "control"], "j", 
        lazy.layout.grow_down(), 
        desc="Grow window down"
    ),
    Key([mod, "control"], "k", 
        lazy.layout.grow_up(), 
        desc="Grow window up"
    ),
    Key([mod], "n", 
        lazy.layout.normalize(), 
        desc="Reset all window sizes"
    ),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "space", 
        lazy.layout.toggle_split(), 
        desc="Toggle between split and unsplit sides of stack"
    ),
    # Launch Terminal
    Key([mod], "Return", 
        lazy.spawn(terminal), 
        desc="Launch terminal"
    ),
    Key([mod, "shift"], "Return", lazy.run_extension(extension.DmenuRun(
        dmenu_prompt="Run:",
        dmenu_font="Ubuntu Bold-11",
        background="#282828",
        foreground="#ebdbb2",
        selected_background="#7c6f64",
        selected_foreground="#ebdbb2",
        dmenu_height=24,  # Only supported by some dmenu forks
        desc="Launch Dmenu",
    ))),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", 
        lazy.next_layout(), 
        desc="Toggle between layouts"
    ),
    Key([mod], "w", 
        lazy.window.kill(), 
        desc="Kill focused window"
    ),
    Key([mod, "shift"], "c", 
        lazy.window.kill(), 
        desc="Kill active window"
    ),
    Key([mod, "shift"], "r", 
        lazy.reload_config(), 
        desc="Reload the config"
    ),
    Key([mod, "shift"], "q", 
        lazy.shutdown(), 
        desc="Shutdown Qtile"
    ),
    Key([mod], "r", 
        lazy.spawncmd(), 
        desc="Spawn a command using a prompt widget"
    ),
]

groups = [Group(i) for i in "12345678"]

for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = switch to & move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + shift + letter of group = move focused window to group
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #     desc="move focused window to group {}".format(i.name)),
        ]
    )

# Layout Theme
layout_theme = {"border_width": 2,
                "margin": 15,
                "border_focus":  colors[11],
                "border_normal": colors[10],
               }

layouts = [
    layout.Columns(**layout_theme),
    layout.Max(),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadTall(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
    layout.Floating(**layout_theme),
]

widget_defaults = dict(
    font="sans bold",
    fontsize=13,
    padding=6,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.CurrentLayoutIcon(
					scale=0.6,
					padding=5,
					use_mask=True,
					foreground=colors[1],
					background=colors[0],
					decorations=[PowerLineDecoration(
						path = 'zig_zag',
						size = 12,
                        )
                    ],
                ),
				widget.GroupBox(
					disable_drag=True,
					highlight_method="block",
                    block_highlight_text_color=colors[1],
                    this_current_screen_border=colors[0],
					active=colors[8],
                    inactive=colors[1],
					foreground=colors[1],
					background=colors[10],
					decorations=[PowerLineDecoration(
						path = 'zig_zag',
						size = 12,
                        )
                    ],
                ),
                widget.Prompt(
					foreground=colors[1],
					background=colors[0],
                ),
                widget.WindowName(
					padding=10,
					foreground=colors[1],
					background=colors[0],
                ),
                widget.Systray(
					icon_size=14,
					padding=5,
					foreground=colors[0],
					background=colors[0],
                ),
                widget.Spacer(
					length=5,
					foreground=colors[0],
					background=colors[0],
					decorations=[PowerLineDecoration(
						path = 'zig_zag',
						size = 12,
                        )
                    ],
                ),
                widget.Wttr(
					format="%c Lipjan  %t  %C", 
					location={'Lipjan': 'Lipjan'}, 
					mouse_callbacks={"Button1": lazy.spawn("alacritty --hold -e curl wttr.in/lipjan")}, 
					units="m", 
					update_interval=300,
					foreground=colors[0],
					background=colors[9],
					decorations=[PowerLineDecoration(
						path = 'zig_zag',
						size = 12,
                        )
                    ],
                ),
                widget.GenPollText(
					func=lambda: subprocess.check_output(os.path.expanduser("~/.local/bin/kernel")).decode("utf-8"), 
					update_interval=60,
					foreground=colors[0],
					background=colors[3],
					decorations=[PowerLineDecoration(
						path = 'zig_zag',
						size = 12,
                        )
                    ],
                ),
                widget.DF(
					format="  {uf}{m}B (Free)",
                    visible_on_warn=False,
                    update_interval=60,
					foreground=colors[0],
					background=colors[8],
					decorations=[PowerLineDecoration(
						path = 'zig_zag',
						size = 12,
                        )
                    ],
                ),
                widget.Memory(
                    format="{MemUsed: .1f}{mm}/{MemTotal: .1f}{mm}",
                    mouse_callbacks={"Button1": lazy.spawn("alacritty -e htop")},
                    measure_mem="G",
                    update_interval=3,
					foreground=colors[0],
					background=colors[6],
					decorations=[PowerLineDecoration(
						path = 'zig_zag',
						size = 12,
                        )
                    ],
                ),
                widget.Battery(
					format="{char} {percent:2.0%} ({hour:d}:{min:02d})", 
					battery=0, 
					charge_char="", 
					discharge_char=" ", 
					full_char="", 
					empty_char="", 
					unknown_char="", 
					show_short_text=False,
					foreground=colors[0],
					background=colors[4],
					decorations=[PowerLineDecoration(
						path = 'zig_zag',
						size = 12,
                        )
                    ],
                ),
                widget.Battery(
					format="{char} {percent:2.0%} ({hour:d}:{min:02d})", 
					battery=1, 
					charge_char="", 
					discharge_char=" ", 
					full_char="", 
					empty_char="", 
					unknown_char="", 
					show_short_text=False,
					foreground=colors[0],
					background=colors[9],
					decorations=[PowerLineDecoration(
						path = 'zig_zag',
						size = 12,
                        )
                    ],
                ),
                widget.Backlight(
					format="{percent:5.0%}", 
					backlight_name="intel_backlight",
                    foreground=colors[0],
					background=colors[5],
					decorations=[PowerLineDecoration(
						path = 'zig_zag',
						size = 12,
                        )
                    ],
                ),
                widget.Volume(
					fmt="  {}",
					foreground=colors[0],
					background=colors[7],
					decorations=[PowerLineDecoration(
						path = 'zig_zag',
						size = 12,
                        )
                    ],
                ),
                widget.Clock(
					format="  %A, %d %B  (%H:%M)",
					foreground=colors[0],
					background=colors[2],
                    mouse_callbacks={"Button1": lazy.spawn("alacritty --hold -e cal")},
					decorations=[PowerLineDecoration(
						path = 'zig_zag',
						size = 12,
                        )
                    ],
				),
                widget.TextBox(
					text="", 
					padding=8,
					mouse_callbacks={"Button1": lazy.spawn("archlinux-logout")},
					foreground=colors[1],
					background=colors[10]
                ),
            ],
            # Bar Configuration:
            size=25, # Bar Height
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ],  **layout_theme
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = False

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# Autostart
@hook.subscribe.startup_once
def start_once():
        home = os.path.expanduser('~')
        subprocess.call([home + '/.config/qtile/autostart.sh'])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
